#include <iostream>

using namespace std;
class tictac
{
public:
    char board[3][3] = {
    { 'x', 'o', ' '}, // [y][x]
    { 'o', ' ', ' '},
    { 'x', ' ', ' '},
    };
   int xcounter;
   int ocounter;
   int score=0;
    char currentPlayer(); // switch x's and o's turn
    bool full();
    int checkwin();
    void display();
    int fillboard();

};
int tictac::checkwin() //checks which character won, at the end of function what do we return???
{
     //row[0]
    if(board[0][0]==board[0][1] && board[0][1]==board[0][2] && board[0][0] == board[0][2])
    {
        if (board[0][0] == 'x')
        {
            return 10;
        }
        else if (board[0][0] == 'o')
        {
            return -10;
        }


    }
    //row [1]
    else if(board[1][0]==board[1][1] && board[1][1]==board[1][2] && board[1][0] == board[1][2])
    {
        if (board[1][0] == 'x')
        {
            return 10;

        }
        else if (board[1][0] == 'o')
        {
            return -10;
        }

    }
    // row[2]
    else if (board[2][0]==board[2][1] && board[2][1]==board[1][2] && board[2][0] == board[1][2])
    {
        if (board[2][0] == 'x')
        {
            return 10;

        }
        else if (board[2][0] == 'o')
        {
            return -10;
        }

    }
    //column[0]
    else if(board[0][0]==board[1][0] && board[1][0]==board[2][0] && board[0][0] == board[2][0] )
    {
        if (board[0][0] == 'x')
        {
            return 10;

        }
        else if (board[0][0] == 'o')
        {

            return -10;
        }
    }

    //column [1]
    else if (board[0][1]==board[1][1] && board[1][1]==board[2][1] && board[0][1] == board[2][1])
    {
        if (board[0][0] == 'x')
        {

            return 10;
        }
        else if (board[0][1] == 'o')
        {

            return -10;
        }

    }
    // column[2]
    else if (board[0][2]==board[1][2] && board[1][2]==board[2][2] && board[0][2] == board[2][2])
    {
        if (board[0][2] == 'x')
        {
            return 10;

        }
        else if (board[0][2] == 'o')
        {
            return -10;
        }

    }

    //diagonal[0][0]
    else if(board[0][0]==board[1][1] && board[1][1]==board[2][2] && board[0][0] == board[2][2] )
    {
        if (board[0][0] == 'x')
        {
            return 10;

        }
        else if (board[0][0] == 'o')
        {
            return -10;
        }

    }

    //diagonal[0][1]
    else if (board[0][2]==board[1][1] && board[1][1]==board[2][0] && board[0][2] == board[2][0])
    {
        if (board[0][2] == 'x')
        {
            return 10;

        }
        else if (board[0][2] == 'o')
        {
            return -10;
        }

    }
    return 0;
}
bool tictac::full() // checks if board is full
{
    for(int y=0; y<3; y++)
    {
        for(int x=0; x<3; x++)
        {
            if (board[y][x] == ' ')
            {
                return false;
            }
        }
    }
    return true;
}
void tictac::display() // for loop to display
{

    for(int y=0; y<3; y++)
    {
        for(int x=0; x<3; x++)
        {
             cout << board[y][x]<< " ";

        }
        cout << endl;
    }
    cout << endl;
}
int tictac::fillboard()
{
    int score;
    if(checkwin()== 10) // terminating statements
    {

        //cout << endl << "x wins" << endl;
        //display();
        return 10;
    }
    else if (checkwin()== -10)
    {

        //cout << endl << "o wins" << endl;
        //display();
        return -10;
    }
    else if (full()==true)
    {

        //cout << endl << "tie" << endl;
        //display();
        return 0;
    }
    else // recursion
    {
        for(int y=0; y<3; y++)
        {
            for(int x=0; x<3; x++)
            {
                if (board[y][x] == ' ')
                {
                    board[y][x] =  currentPlayer();
                    fillboard();
                    display();
                }
            }
        }
    }

}
char tictac::currentPlayer()
{
    int xcounter=0;
    int ocounter=0;
    for(int y=0; y<3; y++)
    {
        for(int x=0; x<3; x++)
        {
            if (board[y][x] == 'x')
            {
                xcounter++;
            }
            else if (board[y][x] == 'o')
            {
                ocounter++;
            }

        }
    }
    //cout<< endl << xcounter << " " << ocounter <<endl;

    if (xcounter>ocounter)
    {

        return 'o';

    }
    else if (ocounter > xcounter)
    {
        return 'x';
    }
    else
    {
        return 'x';
    }

}

int main()
{
    tictac game; // initialize tictac class, carries all the variables and functions within the class
    game.display();
    cout << "Recursion: "<< endl;
    game.fillboard();







    return 0;
}

